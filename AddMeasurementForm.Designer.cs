﻿namespace Projekt
{
    partial class AddMeasurementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMeasurementForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_time = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_addMeasurement = new System.Windows.Forms.Button();
            this.tb_pressure1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_pressure2 = new System.Windows.Forms.TextBox();
            this.tb_pulse = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::Projekt.Properties.Resources.login1;
            this.pictureBox1.InitialImage = global::Projekt.Properties.Resources.login1;
            this.pictureBox1.Location = new System.Drawing.Point(282, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(357, 177);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // lbl_time
            // 
            this.lbl_time.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Lucida Console", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_time.Location = new System.Drawing.Point(168, 236);
            this.lbl_time.MinimumSize = new System.Drawing.Size(533, 0);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(533, 37);
            this.lbl_time.TabIndex = 9;
            this.lbl_time.Text = "Pomiar ciśnienia";
            this.lbl_time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpDate
            // 
            this.dtpDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDate.CalendarFont = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDate.Font = new System.Drawing.Font("Lucida Console", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDate.Location = new System.Drawing.Point(175, 304);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(568, 31);
            this.dtpDate.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(152, 400);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 37);
            this.label1.TabIndex = 14;
            this.label1.Text = "Ciśnienie: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_addMeasurement
            // 
            this.btn_addMeasurement.BackColor = System.Drawing.Color.DimGray;
            this.btn_addMeasurement.Font = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_addMeasurement.ForeColor = System.Drawing.Color.LightGray;
            this.btn_addMeasurement.Location = new System.Drawing.Point(329, 597);
            this.btn_addMeasurement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_addMeasurement.Name = "btn_addMeasurement";
            this.btn_addMeasurement.Size = new System.Drawing.Size(259, 82);
            this.btn_addMeasurement.TabIndex = 4;
            this.btn_addMeasurement.Text = "Potwierdź";
            this.btn_addMeasurement.UseVisualStyleBackColor = false;
            this.btn_addMeasurement.Click += new System.EventHandler(this.btn_addMeasurement_Click);
            // 
            // tb_pressure1
            // 
            this.tb_pressure1.BackColor = System.Drawing.Color.DimGray;
            this.tb_pressure1.Font = new System.Drawing.Font("Lucida Console", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_pressure1.ForeColor = System.Drawing.Color.LightGray;
            this.tb_pressure1.Location = new System.Drawing.Point(391, 393);
            this.tb_pressure1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_pressure1.Name = "tb_pressure1";
            this.tb_pressure1.Size = new System.Drawing.Size(149, 47);
            this.tb_pressure1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Console", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(549, 400);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 37);
            this.label2.TabIndex = 19;
            this.label2.Text = "/";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tb_pressure2
            // 
            this.tb_pressure2.BackColor = System.Drawing.Color.DimGray;
            this.tb_pressure2.Font = new System.Drawing.Font("Lucida Console", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_pressure2.ForeColor = System.Drawing.Color.LightGray;
            this.tb_pressure2.Location = new System.Drawing.Point(594, 393);
            this.tb_pressure2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_pressure2.Name = "tb_pressure2";
            this.tb_pressure2.Size = new System.Drawing.Size(149, 47);
            this.tb_pressure2.TabIndex = 2;
            // 
            // tb_pulse
            // 
            this.tb_pulse.BackColor = System.Drawing.Color.DimGray;
            this.tb_pulse.Font = new System.Drawing.Font("Lucida Console", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_pulse.ForeColor = System.Drawing.Color.LightGray;
            this.tb_pulse.Location = new System.Drawing.Point(391, 495);
            this.tb_pulse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_pulse.Name = "tb_pulse";
            this.tb_pulse.Size = new System.Drawing.Size(175, 47);
            this.tb_pulse.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Console", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(240, 502);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 37);
            this.label4.TabIndex = 21;
            this.label4.Text = "Tętno:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddMeasurementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(887, 775);
            this.Controls.Add(this.tb_pulse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_pressure2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_pressure1);
            this.Controls.Add(this.btn_addMeasurement);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lbl_time);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.Color.Ivory;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "AddMeasurementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj pomiar";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_addMeasurement;
        private System.Windows.Forms.TextBox tb_pressure1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_pressure2;
        private System.Windows.Forms.TextBox tb_pulse;
        private System.Windows.Forms.Label label4;
    }
}