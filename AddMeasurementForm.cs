﻿using System;
using System.Windows.Forms;

namespace Projekt
{
    public partial class AddMeasurementForm : Form
    {
        String measurementTime;
        int id;
        int day, month, year;
        SchedulerForm schedulerForm;
        String dataFromCell;
        private bool isDisposeForm = false;


        public AddMeasurementForm(String measurementTime, int id, int day, int month, int year, SchedulerForm schedulerForm)
        {
            this.id = id;
            this.measurementTime = measurementTime;
            this.day = day;
            this.month = month;
            this.year = year;
            this.schedulerForm = schedulerForm;
            InitializeComponent();
            changeProperties();
        }

        public AddMeasurementForm(String measurementTime, int id, int day, int month, int year, SchedulerForm schedulerForm, String dataFromCell)
        {
            this.id = id;
            this.dataFromCell = dataFromCell;
            this.measurementTime = measurementTime;
            this.day = day;
            this.month = month;
            this.year = year;
            this.schedulerForm = schedulerForm;
            InitializeComponent();
            changeProperties();
        }

        private void changeProperties()
        {
            dtpDate.Value = new DateTime(year, month, day);
            lbl_time.Text = "Godzina mierzenia: "+measurementTime;
        }

        private void Accept()
        {
            bool isCorrect = true;

            String date = dtpDate.Value.ToString("dd-MM-yyyy");
            String[] timeLbl = lbl_time.Text.Split(' ');
            String time = timeLbl[2];
            int idUser = User.idUser;

            int systolicPressure = -1;
            int diastolicPressure = -1;
            int pulse = -1;
            try
            {
                systolicPressure = Convert.ToInt32(tb_pressure1.Text);
                diastolicPressure = Convert.ToInt32(tb_pressure2.Text);
                pulse = Convert.ToInt32(tb_pulse.Text);
                isCorrect = true;
            }
            catch
            {
                MessageBox.Show("Uzupełnij prawidłowo wszystkie dane!");
                isCorrect = false;
            }

            if (isCorrect == true)
            {
                Measurement.Insert(date, time, systolicPressure, diastolicPressure, pulse, idUser);
                MessageBox.Show("Dodano pomiar " + systolicPressure + "/" + diastolicPressure + " puls: " + pulse + " w dniu " + date, "Dodano pomiar");
                isDisposeForm = true;
            }
        }

        private void btn_addMeasurement_Click(object sender, EventArgs e)
        {
            using (LoadingForm lfrm = new LoadingForm(Accept))
            {
                lfrm.ShowDialog(this);
            }
            if (isDisposeForm)
            {
                this.Dispose();
                schedulerForm.createTable(month, year);
            }
        }

    }
}
