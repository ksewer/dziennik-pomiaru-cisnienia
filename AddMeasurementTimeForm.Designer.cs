﻿namespace Projekt
{
    partial class AddMeasurementTimeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMeasurementTimeForm));
            this.dtpTime = new System.Windows.Forms.DateTimePicker();
            this.btn_addMeasurementTime = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dtpTime
            // 
            this.dtpTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpTime.Location = new System.Drawing.Point(134, 50);
            this.dtpTime.Name = "dtpTime";
            this.dtpTime.Size = new System.Drawing.Size(372, 45);
            this.dtpTime.TabIndex = 0;
            // 
            // btn_addMeasurementTime
            // 
            this.btn_addMeasurementTime.BackColor = System.Drawing.Color.DimGray;
            this.btn_addMeasurementTime.Font = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_addMeasurementTime.ForeColor = System.Drawing.Color.LightGray;
            this.btn_addMeasurementTime.Location = new System.Drawing.Point(194, 150);
            this.btn_addMeasurementTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_addMeasurementTime.Name = "btn_addMeasurementTime";
            this.btn_addMeasurementTime.Size = new System.Drawing.Size(259, 82);
            this.btn_addMeasurementTime.TabIndex = 17;
            this.btn_addMeasurementTime.Text = "Potwierdź";
            this.btn_addMeasurementTime.UseVisualStyleBackColor = false;
            this.btn_addMeasurementTime.Click += new System.EventHandler(this.btn_addMeasurementTime_Click);
            // 
            // AddMeasurementTimeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(656, 268);
            this.Controls.Add(this.btn_addMeasurementTime);
            this.Controls.Add(this.dtpTime);
            this.ForeColor = System.Drawing.Color.Ivory;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddMeasurementTimeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj godzinę pomiaru";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpTime;
        private System.Windows.Forms.Button btn_addMeasurementTime;
    }
}