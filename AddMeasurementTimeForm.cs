﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class AddMeasurementTimeForm : Form
    {
        private bool isDisposeForm = false;
        public AddMeasurementTimeForm()
        {
            InitializeComponent();
            dtpTime.Format = DateTimePickerFormat.Custom;
            dtpTime.CustomFormat = "HH:mm";
            dtpTime.ShowUpDown = true;
        }

        private void Accept()
        {
            String time = dtpTime.Value.ToString("HH:mm");
            bool isUnique = true;
            List<MeasurementTime> measurementTimes = MeasurementTime.getMeasurementsTimeByUserId(User.idUser);

            foreach (MeasurementTime m in measurementTimes)
            {
                if (m.Time == time)
                {
                    MessageBox.Show("Podana przez Ciebie godzina jest już na liście.", "Błąd dodawania godziny.");
                    isUnique = false;
                    break;
                }
            }
            if (isUnique == true)
            {
                MeasurementTime.Insert(time, User.idUser);
                isDisposeForm = true;
            }
        }

        private void btn_addMeasurementTime_Click(object sender, EventArgs e)
        {
            using (LoadingForm lfrm = new LoadingForm(Accept))
            {
                lfrm.ShowDialog(this);
            }
            if(isDisposeForm == true)
            {
                this.Dispose();
            }
        }
    }
}
