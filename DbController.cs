﻿using MySql.Data.MySqlClient;
using System;

namespace Projekt
{
    class DbController
    {
        //darmowa baza danych
        //private const String SERVER = "85.10.205.173";
        //private const String UID = "capsel";
        //private const String PASSWORD = "capselrOOt1063";

        //localhost
        //private const String SERVER = "127.0.0.1";
        //private const String UID = "root";
        //private const String PASSWORD = "";

        //private const String DATABASE = "m1063_chdb";

        //home.pl
        private const String SERVER = "46.242.240.184";
        private const String UID = "32018658_dziennik_cisnienia";
        private const String PASSWORD = "DziennikPomiaruCisnienia2019#";

        private const String DATABASE = "32018658_dziennik_cisnienia";


        private static MySqlConnection dbConn;

        public static MySqlConnection DbConn { get => dbConn; set => dbConn = value; }

        public static void InitializeDB()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Server = SERVER;
            builder.UserID = UID;
            builder.Password = PASSWORD;
            builder.Database = DATABASE;
            builder.OldGuids = true;

            String connString = builder.ToString();
            builder = null;

            dbConn = new MySqlConnection(connString);
        }
    }
}
