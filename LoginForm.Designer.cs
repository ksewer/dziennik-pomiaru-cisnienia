﻿namespace Projekt
{
    partial class LoginForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.btn_log_in = new System.Windows.Forms.Button();
            this.tb_login = new System.Windows.Forms.TextBox();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.btn_signUp = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_log_in
            // 
            this.btn_log_in.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_log_in.BackColor = System.Drawing.Color.DimGray;
            this.btn_log_in.Font = new System.Drawing.Font("Forte", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_log_in.ForeColor = System.Drawing.Color.LightGray;
            this.btn_log_in.Location = new System.Drawing.Point(264, 457);
            this.btn_log_in.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_log_in.Name = "btn_log_in";
            this.btn_log_in.Size = new System.Drawing.Size(248, 54);
            this.btn_log_in.TabIndex = 3;
            this.btn_log_in.Text = "Zaloguj";
            this.btn_log_in.UseVisualStyleBackColor = false;
            this.btn_log_in.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_login
            // 
            this.tb_login.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_login.BackColor = System.Drawing.Color.DimGray;
            this.tb_login.Font = new System.Drawing.Font("Lucida Console", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_login.ForeColor = System.Drawing.Color.LightGray;
            this.tb_login.Location = new System.Drawing.Point(161, 330);
            this.tb_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_login.Name = "tb_login";
            this.tb_login.Size = new System.Drawing.Size(447, 34);
            this.tb_login.TabIndex = 1;
            this.tb_login.Text = "Login";
            this.tb_login.Enter += new System.EventHandler(this.tb_login_Enter);
            this.tb_login.Leave += new System.EventHandler(this.tb_login_Leave);
            // 
            // tb_password
            // 
            this.tb_password.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_password.BackColor = System.Drawing.SystemColors.GrayText;
            this.tb_password.Font = new System.Drawing.Font("Lucida Console", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_password.ForeColor = System.Drawing.Color.LightGray;
            this.tb_password.Location = new System.Drawing.Point(161, 384);
            this.tb_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(447, 34);
            this.tb_password.TabIndex = 2;
            this.tb_password.Text = "Hasło";
            this.tb_password.Enter += new System.EventHandler(this.tb_password_Enter);
            this.tb_password.Leave += new System.EventHandler(this.tb_password_Leave);
            // 
            // btn_signUp
            // 
            this.btn_signUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_signUp.BackColor = System.Drawing.Color.DimGray;
            this.btn_signUp.Font = new System.Drawing.Font("Forte", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_signUp.ForeColor = System.Drawing.Color.LightGray;
            this.btn_signUp.Location = new System.Drawing.Point(524, 11);
            this.btn_signUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_signUp.Name = "btn_signUp";
            this.btn_signUp.Size = new System.Drawing.Size(248, 54);
            this.btn_signUp.TabIndex = 0;
            this.btn_signUp.Text = "Zarejestruj";
            this.btn_signUp.UseVisualStyleBackColor = false;
            this.btn_signUp.Click += new System.EventHandler(this.btn_signUp_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::Projekt.Properties.Resources.login1;
            this.pictureBox1.InitialImage = global::Projekt.Properties.Resources.login1;
            this.pictureBox1.Location = new System.Drawing.Point(191, 110);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(395, 194);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(784, 636);
            this.Controls.Add(this.btn_signUp);
            this.Controls.Add(this.btn_log_in);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tb_password);
            this.Controls.Add(this.tb_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dziennik pomiarów ciśnienia";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_login;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.Button btn_log_in;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_signUp;
    }
}

