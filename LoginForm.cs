﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Projekt
{
    public partial class LoginForm : Form
    {
        private bool isCorrectLoginAndPassword = false;
        public LoginForm()
        {
            InitializeComponent();
            DbController.InitializeDB();
            clearTextBoxes();
        }

        private void tb_login_Enter(object sender, EventArgs e)
        {
            if (tb_login.Text == "Login")
            {
                tb_login.Text = "";
            }
        }

        private bool checkUser(String login, String password)
        {
            User user = User.getUserByLogin(login);
            if (user != null)
            {
                if (user.Password == password)
                {
                    User.idUser = user.Id;
                    return true;
                }
            }
            return false;
        }

        private void tb_login_Leave(object sender, EventArgs e)
        {
            if (tb_login.Text == "")
            {
                tb_login.Text = "Login";
            }
        }

        private void tb_password_Enter(object sender, EventArgs e)
        {
            if (tb_password.Text == "Hasło")
            {
                tb_password.Text = "";
                tb_password.UseSystemPasswordChar = true;
            }
        }

        private void tb_password_Leave(object sender, EventArgs e)
        {
            if (tb_password.Text == "")
            {
                tb_password.Text = "Hasło";
                tb_password.UseSystemPasswordChar = false;
            }
        }
        private void Accept()
        {
            if (checkUser(tb_login.Text, tb_password.Text))
            {
                try
                {
                    User user = User.getUserByLogin(tb_login.Text);
                    isCorrectLoginAndPassword = true;
                }
                catch
                {
                    MessageBox.Show("Błąd połączenia z bazą danych. " +
                        "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.");
                    isCorrectLoginAndPassword = false;
                }
            }
            else
            {
                MessageBox.Show("Login lub hasło są nieprawidłowe.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (LoadingForm lfrm = new LoadingForm(Accept))
            {
                lfrm.ShowDialog(this);
            }
            if (isCorrectLoginAndPassword == true)
            {
                SchedulerForm form2 = new SchedulerForm();
                form2.Closed += (s, args) => this.Close();
                form2.Show();
                this.Hide();
            }
        }

        public void clearTextBoxes()
        {
            tb_login.Text = "Login";
            tb_password.Text = "Hasło";
            btn_log_in.Focus();
        }

        private void btn_signUp_Click(object sender, EventArgs e)
        {
            SignUp form2 = new SignUp();
            form2.Closed += (s, args) => this.Close();
            form2.Show();
            this.Hide();
        }
    }
}
