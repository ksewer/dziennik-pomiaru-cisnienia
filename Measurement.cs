﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    class Measurement
    {
        private int id;
        private String date;
        private String time;
        private int systolicPressure;
        private int diastolicPressure;
        private int pulse;
        private int idUser;

        public int Id { get => id; set => id = value; }
        public string Date { get => date; set => date = value; }
        public string Time { get => time; set => time = value; }
        public int SystolicPressure { get => systolicPressure; set => systolicPressure = value; }
        public int DiastolicPressure { get => diastolicPressure; set => diastolicPressure = value; }
        public int Pulse { get => pulse; set => pulse = value; }
        public int IdUser { get => idUser; set => idUser = value; }

        private Measurement(int id, String date, String time, int systolicPressure, int diastolicPressure, int pulse, int idUser)
        {
            this.id = id;
            this.date = date;
            this.time = time;
            this.systolicPressure = systolicPressure;
            this.diastolicPressure = diastolicPressure;
            this.pulse = pulse;
            this.idUser = idUser;
        }

        public Measurement(String date, String time, int systolicPressure, int diastolicPressure, int pulse, int idUser)
        {
            this.date = date;
            this.time = time;
            this.systolicPressure = systolicPressure;
            this.diastolicPressure = diastolicPressure;
            this.pulse = pulse;
            this.idUser = idUser;
        }

        public static List<Measurement> getMeasurements()
        {
            List<Measurement> measurements = new List<Measurement>();

            String query = "SELECT * FROM pomiar";

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Measurement measurement = new Measurement(
                        (int)reader["POM_ID"],
                        (String)reader["POM_DATA"],
                        (String)reader["POM_GODZINA"],
                        (int)reader["POM_CIS_SKURCZOWE"],
                        (int)reader["POM_CIS_ROZKURCZOWE"],
                        (int)reader["POM_TETNO"],
                        (int)reader["UZY_ID_fk"]
                        );
                    measurements.Add(measurement);
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }

            return measurements;
        }

        public static List<Measurement> getMeasurementsByIdUser(int id)
        {
            List<Measurement> measurements = new List<Measurement>();

            String query = string.Format("SELECT * FROM pomiar WHERE UZY_ID_fk = '{0}'", id);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Measurement measurement = new Measurement(
                        (int)reader["POM_ID"],
                        (String)reader["POM_DATA"],
                        (String)reader["POM_GODZINA"],
                        (int)reader["POM_CIS_SKURCZOWE"],
                        (int)reader["POM_CIS_ROZKURCZOWE"],
                        (int)reader["POM_TETNO"],
                        (int)reader["UZY_ID_fk"]
                        );
                    measurements.Add(measurement);
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }

            return measurements;
        }

        public static void Insert(String date, String time, int systolicPressure, int diastolicPressure, int pulse, int idUser)
        {
            String query = string.Format("INSERT INTO pomiar (POM_DATA, POM_GODZINA, POM_CIS_SKURCZOWE, POM_CIS_ROZKURCZOWE, POM_TETNO, UZY_ID_fk) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", date, time, systolicPressure, diastolicPressure, pulse, idUser);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }

        public static void Update(String date, String time, int systolicPressure, int diastolicPressure, int pulse, int idUser)
        {
            String query = string.Format("UPDATE pomiar SET POM_GODZINA='{0}', POM_CIS_SKURCZOWE='{1}', POM_CIS_ROZKURCZOWE='{2}', POM_TETNO='{3}' WHERE UZY_ID_fk='{4}' AND POM_DATA='{5}'", time, systolicPressure, diastolicPressure, pulse, idUser, date);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }

        public static void Delete(int idUser, String date)
        {
            String query = string.Format("DELETE FROM pomiar WHERE UZY_ID_fk='{0}' AND POM_DATA='{1}';", idUser, date);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }
    }
}
