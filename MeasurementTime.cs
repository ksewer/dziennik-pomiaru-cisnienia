﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    class MeasurementTime : IComparable<MeasurementTime>
    {
        private int id;
        private String time;
        private int idUser;

        public int Id { get => id; set => id = value; }
        public string Time { get => time; set => time = value; }
        public int IdUser { get => idUser; set => idUser = value; }

        MeasurementTime(int id, String time, int idUser)
        {
            this.id = id;
            this.time = time;
            this.idUser = idUser;
        }

        public static List<MeasurementTime> getMeasurementsTime()
        {
            List<MeasurementTime> measurementsTime = new List<MeasurementTime>();

            String query = "SELECT * FROM godziny_pomiaru";

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MeasurementTime measurementTime = new MeasurementTime(
                        (int)reader["GOD_ID"],
                        (String)reader["GOD_godzina"],
                        (int)reader["UZY_ID_fk"]
                        );

                    measurementsTime.Add(measurementTime);
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
            return measurementsTime;
        }

        public static List<MeasurementTime> getMeasurementsTimeByUserId(int idUser)
        {
            List<MeasurementTime> measurementsTime = new List<MeasurementTime>();

            String query = "SELECT * FROM godziny_pomiaru where UZY_ID_fk = " + idUser;

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MeasurementTime measurementTime = new MeasurementTime(
                        (int)reader["GOD_ID"],
                        (String)reader["GOD_godzina"],
                        (int)reader["UZY_ID_fk"]
                        );

                    measurementsTime.Add(measurementTime);
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
            return measurementsTime;
        }

        public static void Insert(String time, int idUser)
        {
            String query = string.Format("INSERT INTO godziny_pomiaru (GOD_godzina, UZY_ID_fk) VALUES ('{0}', '{1}')", time, idUser);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }
            }
        }
        public static void Delete(int id)
        {
            String query = string.Format("DELETE FROM godziny_pomiaru WHERE GOD_ID='{0}'", id);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();
                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }

        public int CompareTo(MeasurementTime other)
        {
            int result = this.time.CompareTo(other.time);
            return result;
        }
    }
}
