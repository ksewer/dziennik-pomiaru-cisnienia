﻿namespace Projekt
{
    partial class MeasurementTimesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasurementTimesForm));
            this.lvTimes = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_log_out = new System.Windows.Forms.Button();
            this.btn_showAddMeasurementTime = new System.Windows.Forms.Button();
            this.btn_deleteMeasurementTime = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.przejdźDoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grafikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wylogujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lvTimes
            // 
            this.lvTimes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTimes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.time});
            this.lvTimes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvTimes.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lvTimes.FullRowSelect = true;
            this.lvTimes.GridLines = true;
            this.lvTimes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvTimes.HideSelection = false;
            this.lvTimes.Location = new System.Drawing.Point(56, 55);
            this.lvTimes.MultiSelect = false;
            this.lvTimes.Name = "lvTimes";
            this.lvTimes.Size = new System.Drawing.Size(456, 534);
            this.lvTimes.TabIndex = 1;
            this.lvTimes.UseCompatibleStateImageBehavior = false;
            this.lvTimes.View = System.Windows.Forms.View.Details;
            // 
            // id
            // 
            this.id.Text = "ID";
            this.id.Width = 150;
            // 
            // time
            // 
            this.time.Text = "Godziny pomiaru";
            this.time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.time.Width = 300;
            // 
            // btn_log_out
            // 
            this.btn_log_out.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_log_out.BackColor = System.Drawing.Color.DimGray;
            this.btn_log_out.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_log_out.ForeColor = System.Drawing.Color.LightGray;
            this.btn_log_out.Location = new System.Drawing.Point(599, 532);
            this.btn_log_out.Name = "btn_log_out";
            this.btn_log_out.Size = new System.Drawing.Size(171, 57);
            this.btn_log_out.TabIndex = 2;
            this.btn_log_out.Text = "Wyloguj";
            this.btn_log_out.UseVisualStyleBackColor = false;
            this.btn_log_out.Click += new System.EventHandler(this.btn_log_out_Click);
            // 
            // btn_showAddMeasurementTime
            // 
            this.btn_showAddMeasurementTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_showAddMeasurementTime.BackColor = System.Drawing.Color.DimGray;
            this.btn_showAddMeasurementTime.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_showAddMeasurementTime.ForeColor = System.Drawing.Color.LightGray;
            this.btn_showAddMeasurementTime.Location = new System.Drawing.Point(45, 41);
            this.btn_showAddMeasurementTime.Name = "btn_showAddMeasurementTime";
            this.btn_showAddMeasurementTime.Size = new System.Drawing.Size(171, 89);
            this.btn_showAddMeasurementTime.TabIndex = 3;
            this.btn_showAddMeasurementTime.Text = "Dodaj godzine pomiaru";
            this.btn_showAddMeasurementTime.UseVisualStyleBackColor = false;
            this.btn_showAddMeasurementTime.Click += new System.EventHandler(this.btn_showAddMeasurementTime_Click);
            // 
            // btn_deleteMeasurementTime
            // 
            this.btn_deleteMeasurementTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_deleteMeasurementTime.BackColor = System.Drawing.Color.DimGray;
            this.btn_deleteMeasurementTime.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_deleteMeasurementTime.ForeColor = System.Drawing.Color.LightGray;
            this.btn_deleteMeasurementTime.Location = new System.Drawing.Point(45, 162);
            this.btn_deleteMeasurementTime.Name = "btn_deleteMeasurementTime";
            this.btn_deleteMeasurementTime.Size = new System.Drawing.Size(171, 91);
            this.btn_deleteMeasurementTime.TabIndex = 5;
            this.btn_deleteMeasurementTime.Text = "Usuń godzine pomiaru";
            this.btn_deleteMeasurementTime.UseVisualStyleBackColor = false;
            this.btn_deleteMeasurementTime.Click += new System.EventHandler(this.btn_deleteTime_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btn_deleteMeasurementTime);
            this.panel1.Controls.Add(this.btn_showAddMeasurementTime);
            this.panel1.Location = new System.Drawing.Point(554, 211);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 269);
            this.panel1.TabIndex = 7;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.przejdźDoToolStripMenuItem,
            this.wylogujToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(831, 28);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zamknijToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(145, 26);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // przejdźDoToolStripMenuItem
            // 
            this.przejdźDoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grafikToolStripMenuItem});
            this.przejdźDoToolStripMenuItem.Name = "przejdźDoToolStripMenuItem";
            this.przejdźDoToolStripMenuItem.Size = new System.Drawing.Size(102, 24);
            this.przejdźDoToolStripMenuItem.Text = "Przejdź do...";
            // 
            // grafikToolStripMenuItem
            // 
            this.grafikToolStripMenuItem.Name = "grafikToolStripMenuItem";
            this.grafikToolStripMenuItem.Size = new System.Drawing.Size(282, 26);
            this.grafikToolStripMenuItem.Text = "Dziennik pomiarów ciśnienia";
            this.grafikToolStripMenuItem.Click += new System.EventHandler(this.grafikToolStripMenuItem_Click);
            // 
            // wylogujToolStripMenuItem
            // 
            this.wylogujToolStripMenuItem.Name = "wylogujToolStripMenuItem";
            this.wylogujToolStripMenuItem.Size = new System.Drawing.Size(78, 24);
            this.wylogujToolStripMenuItem.Text = "Wyloguj";
            this.wylogujToolStripMenuItem.Click += new System.EventHandler(this.wylogujToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Projekt.Properties.Resources.login1;
            this.pictureBox1.InitialImage = global::Projekt.Properties.Resources.login;
            this.pictureBox1.Location = new System.Drawing.Point(552, 55);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(268, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // MeasurementTimesForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(831, 618);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_log_out);
            this.Controls.Add(this.lvTimes);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MeasurementTimesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stałe godziny pomiarów";
            this.panel1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lvTimes;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.ColumnHeader time;
        private System.Windows.Forms.Button btn_log_out;
        private System.Windows.Forms.Button btn_showAddMeasurementTime;
        private System.Windows.Forms.Button btn_deleteMeasurementTime;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem przejdźDoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grafikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wylogujToolStripMenuItem;
    }
}