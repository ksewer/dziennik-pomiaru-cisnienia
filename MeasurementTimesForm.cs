﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class MeasurementTimesForm : Form
    {
        List<MeasurementTime> measurements;
        List<MeasurementTime> measurementsList = new List<MeasurementTime>();
        LoginForm form1 = new LoginForm();

        public MeasurementTimesForm()
        {
            InitializeComponent();
            loadAllRecords();
        }

        private void setMeasurementsTime()
        {
            measurements = MeasurementTime.getMeasurementsTime();

            measurements.Sort();
        }

        public void loadAllRecords()
        {
            lvTimes.Items.Clear();
            measurementsList.Clear();

            using (LoadingForm lfrm = new LoadingForm(setMeasurementsTime))
            {
                lfrm.ShowDialog(this);
            }

            foreach (MeasurementTime m in measurements)
            {
                if(m.IdUser == User.idUser)
                {
                    measurementsList.Add(m);
                    ListViewItem item = new ListViewItem(measurementsList.Count().ToString());
                    item.SubItems.Add(m.Time);
                    lvTimes.Items.Add(item);
                }
            }
        }

        private void btn_log_out_Click(object sender, EventArgs e)
        {
            form1.Closed += (s, args) => this.Close();
            form1.Show();
            this.Hide();
            form1.clearTextBoxes();
        }

        

        private void btn_deleteTime_Click(object sender, EventArgs e)
        {
            try
            {
                ListViewItem item = lvTimes.SelectedItems[0];
                int id = measurementsList.ElementAt(Convert.ToInt32(item.SubItems[0].Text)-1).Id;
                String text = string.Format("Czy jesteś pewny, że chcesz usunąć godzinę {0} ze swoich pomiarów?", item.SubItems[1].Text);
                DialogResult dialogResult = MessageBox.Show(text, "Usuwanie godziny", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    MeasurementTime.Delete(id);
                    loadAllRecords();
                }
            }
            catch
            {
                MessageBox.Show("Nie wybrano godziny do usunięcia!");
            }
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grafikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SchedulerForm form2 = new SchedulerForm();
            form2.Closed += (s, args) => this.Close();
            form2.Show();
            this.Hide();
        }

        private void wylogujToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Czy chcesz się wylogować?", "Wylogowanie", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                LoginForm form2 = new LoginForm();
                form2.Closed += (s, args) => this.Close();
                form2.Show();
                this.Hide();
            }
        }

        private void btn_showAddMeasurementTime_Click(object sender, EventArgs e)
        {
            AddMeasurementTimeForm form2 = new AddMeasurementTimeForm();
            form2.ShowDialog();
            loadAllRecords();
        }
    }
}
