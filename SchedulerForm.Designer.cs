﻿namespace Projekt
{
    partial class SchedulerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchedulerForm));
            this.dgv_scheduler = new System.Windows.Forms.DataGridView();
            this.lbl_month = new System.Windows.Forms.Label();
            this.btn_nextMonth = new System.Windows.Forms.Button();
            this.btn_prevMonth = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.przejdźDoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.godzinyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wylogujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scheduler)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_scheduler
            // 
            this.dgv_scheduler.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_scheduler.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_scheduler.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_scheduler.BackgroundColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_scheduler.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_scheduler.ColumnHeadersHeight = 80;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_scheduler.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_scheduler.Location = new System.Drawing.Point(0, 115);
            this.dgv_scheduler.MultiSelect = false;
            this.dgv_scheduler.Name = "dgv_scheduler";
            this.dgv_scheduler.RowHeadersWidth = 120;
            this.dgv_scheduler.Size = new System.Drawing.Size(1252, 744);
            this.dgv_scheduler.TabIndex = 0;
            this.dgv_scheduler.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_scheduler_CellDoubleClick);
            // 
            // lbl_month
            // 
            this.lbl_month.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_month.AutoSize = true;
            this.lbl_month.Font = new System.Drawing.Font("Lucida Console", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_month.Location = new System.Drawing.Point(370, 48);
            this.lbl_month.MinimumSize = new System.Drawing.Size(500, 0);
            this.lbl_month.Name = "lbl_month";
            this.lbl_month.Size = new System.Drawing.Size(500, 35);
            this.lbl_month.TabIndex = 1;
            this.lbl_month.Text = "label1";
            this.lbl_month.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btn_nextMonth
            // 
            this.btn_nextMonth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_nextMonth.BackColor = System.Drawing.Color.DimGray;
            this.btn_nextMonth.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_nextMonth.ForeColor = System.Drawing.Color.LightGray;
            this.btn_nextMonth.Location = new System.Drawing.Point(862, 42);
            this.btn_nextMonth.Name = "btn_nextMonth";
            this.btn_nextMonth.Size = new System.Drawing.Size(171, 57);
            this.btn_nextMonth.TabIndex = 3;
            this.btn_nextMonth.Text = "--->";
            this.btn_nextMonth.UseVisualStyleBackColor = false;
            this.btn_nextMonth.Click += new System.EventHandler(this.btn_nextMonth_Click);
            // 
            // btn_prevMonth
            // 
            this.btn_prevMonth.BackColor = System.Drawing.Color.DimGray;
            this.btn_prevMonth.Font = new System.Drawing.Font("Lucida Console", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_prevMonth.ForeColor = System.Drawing.Color.LightGray;
            this.btn_prevMonth.Location = new System.Drawing.Point(225, 42);
            this.btn_prevMonth.Name = "btn_prevMonth";
            this.btn_prevMonth.Size = new System.Drawing.Size(171, 57);
            this.btn_prevMonth.TabIndex = 4;
            this.btn_prevMonth.Text = "<---";
            this.btn_prevMonth.UseVisualStyleBackColor = false;
            this.btn_prevMonth.Click += new System.EventHandler(this.btn_prevMonth_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.przejdźDoToolStripMenuItem,
            this.wylogujToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1252, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zamknijToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // przejdźDoToolStripMenuItem
            // 
            this.przejdźDoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.godzinyToolStripMenuItem});
            this.przejdźDoToolStripMenuItem.Name = "przejdźDoToolStripMenuItem";
            this.przejdźDoToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.przejdźDoToolStripMenuItem.Text = "Przejdź do...";
            // 
            // godzinyToolStripMenuItem
            // 
            this.godzinyToolStripMenuItem.Name = "godzinyToolStripMenuItem";
            this.godzinyToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.godzinyToolStripMenuItem.Text = "Stałe godziny pomiarów";
            this.godzinyToolStripMenuItem.Click += new System.EventHandler(this.godzinyToolStripMenuItem_Click);
            // 
            // wylogujToolStripMenuItem
            // 
            this.wylogujToolStripMenuItem.Name = "wylogujToolStripMenuItem";
            this.wylogujToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.wylogujToolStripMenuItem.Text = "Wyloguj";
            this.wylogujToolStripMenuItem.Click += new System.EventHandler(this.wylogujToolStripMenuItem_Click);
            // 
            // SchedulerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1252, 859);
            this.Controls.Add(this.btn_prevMonth);
            this.Controls.Add(this.btn_nextMonth);
            this.Controls.Add(this.lbl_month);
            this.Controls.Add(this.dgv_scheduler);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SchedulerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dziennik pomiarów ciśnienia";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dgv_scheduler)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_scheduler;
        private System.Windows.Forms.Label lbl_month;
        private System.Windows.Forms.Button btn_nextMonth;
        private System.Windows.Forms.Button btn_prevMonth;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem przejdźDoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem godzinyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wylogujToolStripMenuItem;
    }
}