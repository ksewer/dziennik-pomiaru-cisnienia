﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Projekt
{
    public partial class SchedulerForm : Form
    {
        private DataTable dataTable = new DataTable();
        private List<MeasurementTime> measurementTimes;


        public SchedulerForm()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {
            int month = Convert.ToInt32(DateTime.Now.ToString("MM"));

            int year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));

            createTable(month, year);
        }

        private void setMeasurementsTime()
        {
            measurementTimes = MeasurementTime.getMeasurementsTime();

            measurementTimes.Sort();
        }

        public void createTable(int month, int year)
        {
            dataTable = new DataTable();
            DateTime dtime = new DateTime(year, month, 1);

            dataTable.Columns.Add("Godziny pomiaru");

            using (LoadingForm lfrm = new LoadingForm(setMeasurementsTime))
            {
                lfrm.ShowDialog(this);
            }

            foreach (MeasurementTime m in measurementTimes)
            {
                if(m.IdUser == User.idUser)
                {
                    dataTable.Rows.Add(string.Format("{0}", m.Time));
                }
            }

            for (int i = 0; i < DateTime.DaysInMonth(year, month); i++)
            {
                dataTable.Columns.Add(i + 1 + " " + TimeConvert.chageEnglishNameDayOfWeekToPolish(dtime.DayOfWeek.ToString()));
                dtime = dtime.AddDays(1);
            }

            changeMonthLabel(month, year);

            dgv_scheduler.DataSource = dataTable;
            dgv_scheduler.ReadOnly = true;

            completeDataInTable(month, year);
        }

        private void changeMonthLabel(int month, int year)
        {
            lbl_month.Text = TimeConvert.changeMonthToString(month) + " " + year;
        }

        private void setMonthToShow(bool isClickedNext)
        {
            String[] monthYear = lbl_month.Text.Split(' ');
            int month = TimeConvert.changeMonthToInt(monthYear[0]);
            int year = Convert.ToInt32(monthYear[1]);
            DateTime dt = new DateTime(year, month, 1);

            if (isClickedNext)
            {
                dt = dt.AddMonths(1);
            }
            else
            {
                dt = dt.AddMonths(-1);
            }
            createTable(dt.Month, dt.Year);
        }

        private void btn_prevMonth_Click(object sender, EventArgs e)
        {
            setMonthToShow(false);
        }

        private void btn_nextMonth_Click(object sender, EventArgs e)
        {
            setMonthToShow(true);
        }

        private void dgv_scheduler_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 0 && e.RowIndex < measurementTimes.Count)
            {
                String cell = dataTable.Rows[e.RowIndex][e.ColumnIndex].ToString();
                if (cell == "")
                {
                    String[] monthYear = lbl_month.Text.Split(' ');
                    AddMeasurementForm form2 = new AddMeasurementForm(dataTable.Rows[e.RowIndex]["Godziny pomiaru"].ToString(), measurementTimes.ElementAt(e.RowIndex).Id, e.ColumnIndex, TimeConvert.changeMonthToInt(monthYear[0]), Convert.ToInt32(monthYear[1]), this);
                    form2.ShowDialog();
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć pomiar " + cell, "Usuwanie pomiaru", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        String[] monthYear = lbl_month.Text.Split(' ');
                        int day = e.ColumnIndex;
                        String date = "";//((day < 10) ? "0" + day : day.ToString) + "-" + ((month < 10) ? "0" + month : month) + "-" + year;
                        if (day < 10)
                        {
                            date += "0" + day;
                        }
                        else
                        {
                            date += day;
                        }
                        date += "-";
                        if (TimeConvert.changeMonthToInt(monthYear[0]) < 10)
                        {
                            date += "0" + TimeConvert.changeMonthToInt(monthYear[0]);
                        }
                        else
                        {
                            date += TimeConvert.changeMonthToInt(monthYear[0]);
                        }
                        date += "-" + Convert.ToInt32(monthYear[1]);

                        Measurement.Delete(User.idUser, date);
                        createTable(TimeConvert.changeMonthToInt(monthYear[0]), Convert.ToInt32(monthYear[1]));
                    }
                }
            }
        }
        private void completeDataInTable(int month, int year)
        {
            List<Measurement> measurements = Measurement.getMeasurementsByIdUser(User.idUser);
            int row = 0;
            int day;
            String[] date;
            foreach (Measurement m in measurements)
            {
                date = m.Date.Split('-');
                day = Convert.ToInt32(date[0]);
                if (Convert.ToInt32(date[1]) == month && Convert.ToInt32(date[2]) == year)
                {
                    for(int i=0; i < dataTable.Rows.Count; i++)
                    {
                        if (m.Time == dataTable.Rows[i][0].ToString())
                        {
                            dataTable.Rows[row][day] = m.SystolicPressure+ " / " + m.DiastolicPressure + "\nPuls: " + m.Pulse;
                            row = 0;
                            break;
                        }
                        else
                        {
                            row++;
                        }

                    }
                }
            }
        }

        private void godzinyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MeasurementTimesForm form2 = new MeasurementTimesForm();
            form2.Closed += (s, args) => this.Close();
            form2.Show();
            this.Hide();
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void wylogujToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Czy chcesz się wylogować?", "Wylogowanie", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                LoginForm form2 = new LoginForm();
                form2.Closed += (s, args) => this.Close();
                form2.Show();
                this.Hide();
            }
        }
    }
}
