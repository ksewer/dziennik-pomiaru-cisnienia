﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Projekt
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
            completeComponents();
        }

        private void completeComponents()
        {
            cb_sex.Items.Add("Kobieta");
            cb_sex.Items.Add("Mężczyzna");
            cb_sex.Text = "Nie wybrano";
            cb_sex.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void addUser(String name, String surname, String birthdate, double growth, double weight, String sex, String login, String password)
        {
            User.Insert(name, surname, birthdate, growth, weight, sex, login, password);
        }

        private void btn_addUser_Click(object sender, EventArgs e)
        {
            if (
                String.IsNullOrEmpty(tb_name.Text) ||
                String.IsNullOrEmpty(tb_surname.Text) ||
                String.IsNullOrEmpty(tb_growth.Text) ||
                String.IsNullOrEmpty(tb_weight.Text) ||
                String.IsNullOrEmpty(tb_login.Text) ||
                cb_sex.SelectedItem == null ||
                String.IsNullOrEmpty(tb_password.Text))
            {
                MessageBox.Show("Nie uzupełniono conajmniej jednego pola!");
            }
            else
            {
                List<User> users = User.getUsers();
                foreach (User u in users)
                {
                    if (tb_login.Text == u.Login)
                    {
                        MessageBox.Show("Użytkownik o podanym loginie już istnieje, wprowadź inny login.");
                        return;
                    }
                }
                try
                {
                    addUser(
                    tb_name.Text,
                    tb_surname.Text,
                    dtp_birthdate.Value.ToString("dd-MM-yyyy"),
                    Convert.ToDouble(tb_growth.Text),
                    Convert.ToDouble(tb_weight.Text),
                    cb_sex.SelectedItem.ToString(),
                    tb_login.Text,
                    tb_password.Text);
                }
                catch
                {
                    MessageBox.Show("Podałeś złe dane, popraw się!");
                }
                
                MessageBox.Show("Użytkownik "+tb_login.Text+" został pomyślnie zarejestrowany.");
                LoginForm form2 = new LoginForm();
                form2.Closed += (s, args) => this.Close();
                form2.Show();
                this.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginForm form2 = new LoginForm();
            form2.Closed += (s, args) => this.Close();
            form2.Show();
            this.Hide();
        }
    }
}
