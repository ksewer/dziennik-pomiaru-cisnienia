﻿using System;

namespace Projekt
{
    class TimeConvert
    {
        public static int changeMonthToInt(String monthS)
        {
            int month = -1;
            switch (monthS)
            {
                case "styczeń":
                    month = 1;
                    break;

                case "luty":
                    month = 2;
                    break;

                case "marzec":
                    month = 3;
                    break;

                case "kwiecień":
                    month = 4;
                    break;

                case "maj":
                    month = 5;
                    break;

                case "czerwiec":
                    month = 6;
                    break;

                case "lipiec":
                    month = 7;
                    break;

                case "sierpień":
                    month = 8;
                    break;

                case "wrzesień":
                    month = 9;
                    break;

                case "październik":
                    month = 10;
                    break;

                case "listopad":
                    month = 11;
                    break;

                case "grudzień":
                    month = 12;
                    break;
            }

            return month;
        }

        public static String changeMonthToString(int month)
        {
            String monthS = "";
            switch (month)
            {
                case 1:
                    monthS = "styczeń";
                    break;

                case 2:
                    monthS = "luty";
                    break;

                case 3:
                    monthS = "marzec";
                    break;

                case 4:
                    monthS = "kwiecień";
                    break;

                case 5:
                    monthS = "maj";
                    break;

                case 6:
                    monthS = "czerwiec";
                    break;

                case 7:
                    monthS = "lipiec";
                    break;

                case 8:
                    monthS = "sierpień";
                    break;

                case 9:
                    monthS = "wrzesień";
                    break;

                case 10:
                    monthS = "październik";
                    break;

                case 11:
                    monthS = "listopad";
                    break;

                case 12:
                    monthS = "grudzień";
                    break;
            }

            return monthS;
        }

        public static String chageEnglishNameDayOfWeekToPolish(String englishDayOfWeek)
        {
            String polishDayOfWeek = null;

            switch (englishDayOfWeek)
            {
                case "Monday":
                    return "poniedziałek";
                case "Tuesday":
                    return "wtorek";
                case "Wednesday":
                    return "środa";
                case "Thursday":
                    return "czwartek";
                case "Friday":
                    return "piątek";
                case "Saturday":
                    return "sobota";
                case "Sunday":
                    return "niedziela";
            }

            return polishDayOfWeek;
        }
    }
}
