﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Projekt
{
    class User
    {
        private int id;
        private String name;
        private String surname;
        private String birthdate;
        private double growth;
        private double weight;
        private String sex;
        private String login;
        private String password;

        public static int idUser;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Birthdate { get => birthdate; set => birthdate = value; }
        public double Growth { get => growth; set => growth = value; }
        public double Weight { get => weight; set => weight = value; }
        public string Sex { get => sex; set => sex = value; }
        public string Login { get => login; set => login = value; }
        public string Password { get => password; set => password = value; }



        private User(int id, String name, String surname, String birthdate, double growth, double weight, String sex, String login, String password)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.birthdate = birthdate;
            this.growth = growth;
            this.weight = weight;
            this.sex = sex;
            this.login = login;
            this.password = password;
        }

        public User(String name, String surname, String birthdate, double growth, double weight, String sex, String login, String password)
        {
            this.name = name;
            this.surname = surname;
            this.birthdate = birthdate;
            this.growth = growth;
            this.weight = weight;
            this.sex = sex;
            this.login = login;
            this.password = password;
        }

        public static List<User> getUsers()
        {
            List<User> users = new List<User>();

            String query = "SELECT * FROM uzytkownik";

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    User user = new User(
                        (int)reader["UZY_ID"],
                        (String)reader["UZY_IMIE"],
                        (String)reader["UZY_NAZWISKO"],
                        (String)reader["UZY_DATA_URODZENIA"],
                        (double)reader["UZY_WZROST"],
                        (double)reader["UZY_WAGA"],
                        (String)reader["UZY_PLEC"],
                        (String)reader["UZY_LOGIN"],
                        (String)reader["UZY_HASLO"]
                        );

                    users.Add(user);
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }

            return users;
        }

        public static User getUserByLogin(String login)
        {
            String query = "SELECT * FROM uzytkownik WHERE UZY_LOGIN = '" + login + "'";
            User user = null;

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            try
            {
                DbController.DbConn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user = new User(
                       (int)reader["UZY_ID"],
                        (String)reader["UZY_IMIE"],
                        (String)reader["UZY_NAZWISKO"],
                        (String)reader["UZY_DATA_URODZENIA"],
                        (double)reader["UZY_WZROST"],
                        (double)reader["UZY_WAGA"],
                        (String)reader["UZY_PLEC"],
                        (String)reader["UZY_LOGIN"],
                        (String)reader["UZY_HASLO"]
                       );
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }

            return user;
        }

        public static User getUserByID(int id)
        {
            User user = null;
            String query = "SELECT * FROM uzytkownik WHERE UZY_ID = '" + id + "'";

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            try
            {
                DbController.DbConn.Open();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user = new User(
                        (int)reader["UZY_ID"],
                        (String)reader["UZY_IMIE"],
                        (String)reader["UZY_NAZWISKO"],
                        (String)reader["UZY_DATA_URODZENIA"],
                        (double)reader["UZY_WZROST"],
                        (double)reader["UZY_WAGA"],
                        (String)reader["UZY_PLEC"],
                        (String)reader["UZY_LOGIN"],
                        (String)reader["UZY_HASLO"]
                       );
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }

            return user;
        }

        public static void Insert(String name, String surname, String birthdate, double growth, double weight, String sex, String login, String password)
        {
            String query = string.Format("INSERT INTO uzytkownik (UZY_IMIE, UZY_NAZWISKO, UZY_DATA_URODZENIA, UZY_WZROST, UZY_WAGA, UZY_PLEC, UZY_LOGIN, UZY_HASLO) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", name, surname, birthdate, growth, weight, sex, login, password);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }

        public static void Update(int id, String name, String surname, String birthdate, double growth, double weight, String sex, String login, String password)
        {
            String query = string.Format("UPDATE uzytkownik SET UZY_IMIE='{0}', UZY_NAZWISKO='{1}', UZY_DATA_URODZENIA='{2}', UZY_WZROST='{3}', UZY_WAGA='{4}', UZY_PLEC='{5}', UZY_LOGIN='{6}', UZY_HASLO='{7}' WHERE PRA_ID='{8}'", name, surname, birthdate, growth, weight, sex, login, password, id);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }

        public static void Delete(int id)
        {
            String query = string.Format("DELETE FROM uzytkownik WHERE UZY_ID='{0}'", id);

            String queryMeasurement = string.Format("DELETE FROM pomiar WHERE UZY_ID_fk='{0}'", id);

            MySqlCommand cmd = new MySqlCommand(query, DbController.DbConn);

            MySqlCommand cmdMeasurement = new MySqlCommand(query, DbController.DbConn);
            try
            {
                DbController.DbConn.Open();

                cmdMeasurement.ExecuteNonQuery();
                cmd.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z bazą danych. " +
                    "Sprawdź czy masz dostęp do internetu i spróbuj ponownie.", "Błąd połączenia z bazą danych ");
            }
            finally
            {
                if (DbController.DbConn.State == System.Data.ConnectionState.Open)
                {
                    DbController.DbConn.Close();
                }

            }
        }
    }
}
